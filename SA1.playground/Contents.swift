//: Playground - noun: a place where people can play

class Node {
    var value: Int?
    var leftChild: Node?
    var rightChild: Node?
    
    init(_ value: Int) {
        self.value = value
    }
    
    init(_ value: Int, left: Node?, right: Node?) {
        self.value = value
        self.leftChild = left
        self.rightChild = right
    }
}

class LoopNode {
    
    var value: Int
    
    var next: LoopNode?
    
    init(value: Int) {
        self.value = value
    }
}

func checkLoop(_ node: LoopNode?) -> Bool
{
    var current = node
    var newOne = node
    
    while (newOne != nil) && (newOne?.next != nil) {
        
        current = current?.next
        newOne = newOne?.next?.next
        
        if newOne === current {
            return true
        }
    }
    
    return false
}

let head: LoopNode = LoopNode(value: 20)

let n1: LoopNode = LoopNode(value: 30)
let n2: LoopNode = LoopNode(value: 40)
let n3: LoopNode = LoopNode(value: 40)

head.next = n1
n1.next = n2
n2.next = n3
n3.next = n1

print("detect Loop: \(checkLoop(head))")

let tree = Node(20, left:Node(10), right:Node(29, left:Node(25), right:nil))


func isBinarySearch(_ node : Node?, min : Int, max: Int) -> Bool {
    
    if let node = node, let value = node.value {
        if value <= min || value > max {
            return false
        }
        let left = isBinarySearch(node.leftChild, min : min, max: value)
        let right = isBinarySearch(node.rightChild, min: value, max: max)
        return left && right
    }
    return true
}

let isBinarySearchTree = isBinarySearch(tree, min: Int.min, max: Int.max)
print("isBinarySearchTree = \(isBinarySearchTree)")
